// The tool searches files under a directory (defaults /tmp) and
// generates bash script for removing files larger than 1MB.
//
// Usage: cleantmp [directory]
//
// Build: go build cleantmp.go
//

package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	var (
		mb int64 = 1 << 20
		gb int64 = mb * 1024
	)

	flag.Parse()

	dirPath := flag.Arg(0)
	if len(dirPath) == 0 {
		dirPath = os.TempDir()
	}

	fmt.Println("#!/bin/sh")

	fileMap := map[string]os.FileInfo{}

	walk := func(filePath string, info os.FileInfo, err error) error {
		if err != nil {
			if len(fileMap) == 0 {
				fmt.Println()
			}
			fmt.Println("# Error:", err)
			return nil
		}
		if info.IsDir() {
			return nil
		}
		if info.Size() > mb {
			fileMap[filePath] = info
		}
		return nil
	}
	filepath.Walk(dirPath, walk)

	fmt.Println()

	for len(fileMap) > 0 {
		maxSize := int64(0)
		maxPath := ""
		for filePath, info := range fileMap {
			size := info.Size()
			if maxSize < size {
				maxSize = size
				maxPath = filePath
			}
		}
		if info, ok := fileMap[maxPath]; ok {
			modDate := info.ModTime().Format("2006-01-02")
			filePath := strings.Replace(maxPath, "'", "\\'", -1)
			if maxSize > gb {
				fmt.Printf("rm -v '%s' # %dGB %s\n", filePath, maxSize/gb, modDate)
			} else if maxSize > mb {
				fmt.Printf("rm -v '%s' # %dMB %s\n", filePath, maxSize/mb, modDate)
			}
		}
		delete(fileMap, maxPath)
	}
}
